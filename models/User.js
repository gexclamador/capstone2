


//[SECTION] Modules and Dependencies
   const mongoose = require('mongoose');

//[SECTION] Schema/Blueprint 
    const userSchema = new mongoose.Schema({
        firstName: {
            type: String,
            required: [false, 'First Name is Required']
        },
        lastName: {
            type: String,
            required: [false, 'Last Name is Required']
        },
        email: {
            type: String,
            required: [true, 'Email is Required']
        },
        password: {
            type: String,
            required: [true, 'Password is Required']
        },
        isAdmin: {
            type: Boolean,
            default: false
        },
        mobileNo: {
            type: String,
            required: [false, 'Mobile Number is Required']
        },
          cart: [
        {
            productId: {
                type: mongoose.Schema.Types.ObjectId,
                ref: 'Product'
            },
            quantity: Number
        }
    ]

    });

//[SECTION] Model
    module.exports = mongoose.model('User', userSchema); 