
const mongoose = require('mongoose');

const orderSchema = new mongoose.Schema({
  userId: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'User', // Reference to the User model (adjust as needed)
    required: [true, 'User ID is required'],
  },
  products: [
    {
      productId: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Product', // Reference to the Product model (adjust as needed)
        required: true,
      },
      quantity: {
        type: Number,
        required: true,
      },
      productName: {
        type: String, // Assuming the product name is a string
        required: false,
      },
    },
  ],
  totalAmount: {
    type: Number,
    required: [true, 'Total amount is required'],
  },
  purchasedOn: {
    type: Date,
    default: Date.now,
  },
});

module.exports = mongoose.model('Order', orderSchema);



// const mongoose = require('mongoose');

// const orderSchema = new mongoose.Schema({

//   userId: {
//     type: mongoose.Schema.Types.ObjectId,
//     ref: 'User', // Reference to the User model (adjust as needed)
//     required: [true, 'User ID is required'],
//   },
//   products: [
//     {
//       productId: {
//         type: mongoose.Schema.Types.ObjectId,
//         ref: 'Product', // Reference to the Product model (adjust as needed)
//         required: true,
//       },
//       quantity: {
//         type: Number,
//         required: true,
//       },
//     },
//   ],
//   totalAmount: {
//     type: Number,
//     required: [true, 'Total amount is required'],
//   },
//   purchasedOn: {
//     type: Date,
//     default: Date.now,
//   },
// });

   
// module.exports = mongoose.model('Order', orderSchema);





