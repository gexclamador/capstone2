const bcrypt = require('bcrypt');
const auth = require('../auth.js');
const Order = require('../models/Order.js');
const Product = require('../models/Product.js');
const User = require('../models/User.js');


module.exports.getAllOrders = (request,response) => {
  return Order.find({}).then(result => {
    return response.send(result);
  })
}

module.exports.createOrder = async (request, response) => {
  if (request.user.isAdmin) {
    return response.send('Action Forbidden');
  }

  const { userId, productId, quantity } = request.body;

  // Convert quantity to a number
  const parsedQuantity = parseInt(quantity, 10);

  if (!userId || !productId || isNaN(parsedQuantity)) {
    return response.send({ message: 'Invalid input data' });
  }

  const user = await User.findById(userId).exec();
  const product = await Product.findById(productId).exec();

  if (!user || !product) {
    return response.send({ message: 'User or product not found' });
  }

  const totalAmount = product.price * parsedQuantity;

  const existingCartItem = user.cart.find(cartItem => cartItem.productId.equals(productId));

  if (existingCartItem) {
    // Update the quantity of the existing cart item
    existingCartItem.quantity += parsedQuantity;
  } else {
    // Add a new cart item
    user.cart.push({ productId: productId, quantity: parsedQuantity, productName: product.name });
  }

  // Create the new order with product details
  const newOrder = new Order({
    userId: user._id,
    products: [
      {
        productId: product._id,
        quantity: parsedQuantity,
        productName: product.name,
        price: product.price,
      },
      ...user.cart.map(cartItem => ({
        productId: cartItem.productId,
        quantity: cartItem.quantity,
        productName: cartItem.productName, // Include product name for cart items
        price: product.price, // Or use cartItem.price if available
      })),
    ],
    totalAmount: totalAmount,
  });

  user.orders = user.orders || [];
  user.orders.push(newOrder._id);

  try {
    await Promise.all([user.save(), product.save(), newOrder.save()]);
    return response.send({ message: 'Order created successfully', order: newOrder });
  } catch (error) {
    return response.status(500).send({ message: error.message });
  }
};



module.exports.addToCart = (request, response) => {
  if (!request.user) {
    return response.status(401).json({ message: 'Authentication required' });
  }

  const { productId } = request.body;

  if (!productId) {
    return response.status(400).json({ message: 'Invalid input data' });
  }

  User.findById(request.user.id)
    .populate('cart.productId') // Populate the product details for each cart item
    .exec()
    .then(user => {
      if (!user) {
        return response.status(404).json({ message: 'User not found' });
      }

      const cartItem = user.cart.find(item => item.productId.equals(productId));

      if (cartItem) {
        cartItem.quantity += 1; // Increment quantity by 1
      } else {
        const orderItem = user.orders.find(order => order.products[0].productId.equals(productId));
        if (orderItem) {
          user.cart.push({ productId: orderItem.products[0].productId, quantity: orderItem.products[0].quantity });
        } else {
          user.cart.push({ productId: productId, quantity: 1 }); // Default quantity if not found in Order
        }
      }

      // Calculate subtotal and total prices
      const subtotal = cartItem.productId.price * cartItem.quantity;

      // Calculate total price by summing up the prices of all items in the cart
      const total = user.cart.reduce((sum, item) => {
        return sum + item.productId.price * item.quantity;
      }, 0);

      return user
        .save()
        .then(() => {
          return response.status(200).json({
            message: 'Product added to cart',
            user,
            subtotal,
            total
          });
        });
    })
    .catch(error => {
      console.error(error);
      return response
        .status(500)
        .json({ message: 'An error occurred while adding the product to cart' });
    });
};





//DELETING CART ITEMS AND CHANGING QUANTITY

module.exports.changeProductQuantity = (request, response) => {
    const { userId, productId, newQuantity } = request.body;

    if (!userId || !productId || !newQuantity) {
      return response.send({ message: 'Invalid input data' });
    }

    User.findById(userId)
      .exec()
      .then(user => {
        if (!user) {
          return response.send({ message: 'User not found' });
        }

        const cartItem = user.cart.find(item => item.productId.equals(productId));

        if (cartItem) {
          cartItem.quantity = newQuantity;
        } else {
          return response.send({ message: 'Product not found in cart' });
        }

        return user.save();
      })
      .then(() => {
        return response.send({ message: 'Product quantity changed successfully' });
      })
      .catch(error => {
        return response.send({ message: error.message });
      });
  };

 module.exports.removeProductFromCart = (request, response) => {
    const { userId, productId } = request.body;

    if (!userId || !productId) {
      return response.send({ message: 'Invalid input data' });
    }

    User.findById(userId)
      .exec()
      .then(user => {
        if (!user) {
          return response.send({ message: 'User not found' });
        }

        const cartItemIndex = user.cart.findIndex(item => item.productId.equals(productId));

        if (cartItemIndex !== -1) {
          user.cart.splice(cartItemIndex, 1);
        } else {
          return response.send({ message: 'Product not found in cart' });
        }

        return user.save();
      })
      .then(() => {
        return response.send({ message: 'Product removed from cart successfully' });
      })
      .catch(error => {
        return response.send({ message: error.message });
      });
  };

