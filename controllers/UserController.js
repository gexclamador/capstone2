const User = require('../models/User.js');
const Order = require('../models/Order.js');
const bcrypt = require('bcrypt');
const auth = require('../auth.js');
const jwt = require('jsonwebtoken');


module.exports.checkEmailExists = (request_body) => {
	return User.find({email: request_body.email}).then((result, error) => {
		if(error){
			return {
				message: error.message 
			}
		}

		if (result.length <= 0){
			return false;
		}
		return true;
	})
}

module.exports.registerUser = (request_body) => {
  let new_user = new User({

 
    firstName: request_body.firstName,
    lastName: request_body.lastName,
    email: request_body.email,
    mobileNo: request_body.mobileNo,
  
    password: bcrypt.hashSync(request_body.password, 10)
  });

  return new_user.save().then((registered_user, error) => {
    if(error){
      return {
        message: error.message
      };
    }

    return {
      message: 'Successfully registered a user!'
    };
  }).catch(error => console.log(error));
}


module.exports.loginUser = (request, response) => {
  return User.findOne({email: request.body.email}).then(result => {
    // Checks if a user is found with an existing email
    if(result == null){
      return response.send({
        message: "The user isn't registered yet."
      }) 
    }
    const isPasswordCorrect = bcrypt.compareSync(request.body.password, result.password);

    if(isPasswordCorrect){
      return response.send({accessToken: auth.createAccessToken(result)});
    } else {
      return response.send({
        message: 'Your password is incorrect.'
      })
    }
  }).catch(error => response.send(error));
}


module.exports.setUserAsAdmin = (userId) => {
  return User.findOneAndUpdate(
    { _id: userId },
    { isAdmin: true },
    { new: true }
  )
  .then((user) => {
    if (!user) {
      return {
        message: 'User not found'
      };
    }
    user.password = "";
    return {
      message: 'User set as admin successfully',
      user: user
    };
  })
  .catch(error => {
    console.log(error);pm
    return {
      message: error.message
    };
  });
};

module.exports.getProfile = (request_body) => {
  return User.findOne({ _id: request_body.id })
    .then((user) => {
      if (!user) {
        return {
          message: 'User not found'
        };
      }

      user.password = "";

      return user;
    })
    .catch(error => {
      console.log(error);
      return {
        message: error.message
      };
    });
};


module.exports.getAuthenticatedUserOrders = (request, response) => {
  const userId = request.user.id; // Get user ID from authenticated user
  return User.findOne({ _id: userId })
    .populate('cart.productId') // Populate cart items with product details
    .then((user) => {
      if (!user) {
        return {
        message: 'User not found'
      };
    }

    const result = {
      message: 'Retrieved user cart successfully',
      cart: user.cart // Include user's cart in the response
    };

    response.status(200).json(result); // Sending the response directly
  })
  .catch((error) => {
    console.log(error);
    const result = {
      message: error.message
    };
    response.status(500).json(result); // Sending the response directly
  });
};











