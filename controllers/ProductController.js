const Product = require('../models/Product.js');
const User = require('../models/User.js');
const bcrypt = require('bcrypt');
const auth = require('../auth.js');
const multer = require('multer');


const storage = multer.diskStorage({
  destination: function (req, file, cb) {
    cb(null, './uploads');
  },
  filename: function (req, file, cb) {
    cb(null, Date.now() + '-' + file.originalname);
  },
});

const upload = multer({ storage: storage });

module.exports.addProduct = (request, response) => {
  upload.single('image')(request, response, (err) => {
    if (err) {
      return response.status(400).json({ error: 'File upload error.' });
    }

    let new_product = new Product({
      name: request.body.name,
      description: request.body.description,
      price: request.body.price,
      image: request.file ? request.file.filename : null, // Use the extracted image filename
    });

    return new_product
      .save()
      .then((saved_product, error) => {
        if (error) {
          return response.send(false);
        }

        return response.send(true);
      })
      .catch((error) => response.send(error));
  });
};


module.exports.updateProduct = (request, response) => {
  upload.single('image')(request, response, (err) => {
    if (err) {
      return response.status(400).json({ error: 'File upload error.' });
    }

    // Create an object to store updated product details
    const update_product_details = {
      name: request.body.name,
      description: request.body.description,
      price: request.body.price,
      image: request.file ? request.file.filename : request.body.image, // Use the extracted image filename or the existing image URL
    };

    Product.findByIdAndUpdate(request.params.id, update_product_details)
      .then((product) => {
        if (!product) {
          return response.status(404).json({ error: 'Product not found.' });
        }

        return response.status(200).json({ message: 'Product updated successfully!' });
      })
      .catch((error) => {
        return response.status(500).json({ error: 'Internal Server Error' });
      });
  });
};


module.exports.getAllProducts = (request,response) => {
	return Product.find({}).then(result => {
		return response.send(result);
	})
}

module.exports.getAllActiveProducts = (request,response) => {
	return Product.find({isActive: true}).then(result => {
		return response.send(result);
	})
}


module.exports.getProduct = (request,response) => {
	return Product.findById(request.params.id).then(result => {
		return response.send(result);
	})
}



module.exports.archiveProduct = (request,response) => {
	return Product.findByIdAndUpdate(request.params.id,{isActive :false}).then((course,error) =>{
			if(error) {
				return response.send(false);
			}
			return response.send(true);
		})
}


module.exports.activateProduct = (request,response) => {
	return Product.findByIdAndUpdate(request.params.id,{isActive : true}).then((course,error) =>{
			if(error) {
				return response.send(false);
			}
			return response.send(true);
		})
}


// Search products by name
module.exports.searchProductsByName = async (request, response) => {
  try {
    const { productName } = request.body;

    const products = await Product.find({
      name: { $regex: productName, $options: 'i' }
    });

    response.status(200).json(products);
  } catch (error) {
    console.error(error);
    response.status(500).json({ error: 'Internal Server Error' });
  }
};


// Search products by price range
module.exports.searchProductsByPriceRange = async (request, response) => {
  try {
    const { minPrice, maxPrice } = request.body;

    const products = await Product.find({
      price: { $gte: minPrice, $lte: maxPrice }
    });

    response.status(200).json(products);
  } catch (error) {
    console.error(error);
    response.status(500).json({ error: 'Internal Server Error' });
  }
};
