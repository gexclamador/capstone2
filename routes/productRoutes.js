const express = require('express')
const router = express.Router();
const auth = require('../auth.js');
const ProductController = require('../controllers/ProductController.js');
const UserController = require('../controllers/UserController.js');
// const OrderController = require('../controllers/OrderController.js'); 



// ROUTES

// Create single product
router.post('/', auth.verify, auth.verifyAdmin, (request, response) => {
	ProductController.addProduct(request, response)
});

//Retrieve all Products
router.get('/all',(request,response) =>{
  ProductController.getAllProducts(request,response);
});

//Get all active products
router.get('/',(request,response) => {
 ProductController.getAllActiveProducts(request,response);

})

//Get Specific product
router.get('/:id',(request,response) => {
 ProductController.getProduct(request,response);

})

//update product
router.put('/:id',auth.verify, auth.verifyAdmin,(request,response) => {
 ProductController.updateProduct(request,response);

})


//Archiving singleproduct
router.put('/:id/archive',auth.verify,auth.verifyAdmin,(request,response) => {
 ProductController.archiveProduct(request,response);

})

//Activate singleproduct
router.put('/:id/activate',auth.verify,auth.verifyAdmin,(request,response) => {
 ProductController.activateProduct(request,response);

})

router.post('/addToCart', auth.verify, (request, response) => {
    console.log('Request Headers:', request.headers);
    console.log('Request Body:', request.body);
    console.log('Authenticated User:', request.user);
    
    ProductController.addToCart(request, response);
});


// Add route to search products by name
router.post('/searchByName', (request, response) => {
  ProductController.searchProductsByName(request, response);
});

// Add route to search products by price range
router.post('/searchByPriceRange', (request, response) => {
  ProductController.searchProductsByPriceRange(request, response);
});


module.exports = router;
