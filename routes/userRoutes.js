const express = require('express');
const router = express.Router();
const UserController = require('../controllers/UserController.js');
const OrderController = require('../controllers/OrderController.js'); 
const auth = require('../auth.js');

// Check if email exists
router.post('/check-email', (request, response) => {
	// Controller function goes here
	UserController.checkEmailExists(request.body).then((result) => {
		response.send(result);
	});
})


// Register user
router.post('/register', (request, response) => {
	UserController.registerUser(request.body).then((result) => {
		response.send(result);
	})
})


// Login user
router.post('/login', (request, response) => {
	UserController.loginUser(request, response);
})

// Get user details
router.post('/details', auth.verify, auth.verifyAdmin, (request, response) => {
	UserController.getProfile(request.body).then((result) => {
		response.send(result);
	})
})


//set user as admin
router.post('/setAdmin', auth.verify, auth.verifyAdmin, (request, response) => {
	UserController.setUserAsAdmin(request.body).then((result) => {
		response.send(result);
	})
})


//get user's orders
router.get('/userOrders', auth.verify, (request, response) => {
  UserController.getAuthenticatedUserOrders(request, response);
});

module.exports = router;