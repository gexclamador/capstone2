const express = require('express');
const router = express.Router();
const auth = require('../auth.js');
const OrderController = require('../controllers/OrderController.js');

// Create order (Non-admin User checkout)
router.post('/checkout', auth.verify, (request, response) => {
     console.log('Checkout route reached');
    OrderController.createOrder(request, response);
});

router.get('/', auth.verify, (request, response) => {
     console.log('Checkout route reached');
    OrderController.getAllOrders(request, response);
});

router.post('/addToCart', auth.verify, (request, response) => {
     console.log('Checkout route reached');
    OrderController.addToCart(request, response);
});


router.post('/quantity', auth.verify, (request, response) => {
     console.log('Checkout route reached');
    OrderController.changeProductQuantity(request, response);
});

router.delete('/removeProduct', auth.verify, (request, response) => {
     console.log('Checkout route reached');
    OrderController.removeProductFromCart(request, response);
});



module.exports = router;
